<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <h1 class="page-header">
            <?php the_title(); ?>
            <small><?php echo get_the_excerpt(); ?> </small>
        </h1>
        <!-- First Blog Post -->
        <h2>
            <a  href="<?php the_permalink();?>"><?php the_title();?></a>
        </h2>
        <p class="lead">
            by <a href="index.php"><?php the_author(); ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php the_time('d.m.Y') ?></p>
        <hr>
            <?php if (has_post_thumbnail()):?>
                <img class="img-responsive" src="<?php the_post_thumbnail();?>" alt="">
            <?php else:?>
                <img class="img-responsive" src="http://placehold.it/900x300" alt="">
            <?php endif;?>

        <hr>
        <p> <?php the_content(); ?></p>
        <a class="btn btn-primary" href="<?php the_permalink();?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

    <?php endwhile; ?>
<?php endif; ?>


<hr>
<!-- Pager -->
<ul class="pager">
    <li class="previous">
        <a href="#">&larr; Older</a>
    </li>
    <li class="next">
        <a href="#">Newer &rarr;</a>
    </li>
</ul>

