 <h4>Blog Search</h4>
    <div class="input-group">
        <form class="form" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="form-group">
            <input type="text" class="form-control" value="" name="s" id="s" />
        </div>
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </form>

    </div>
