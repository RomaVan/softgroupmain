<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'sg'),
) );


/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'left',
        'name' => __('Left sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
}


/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}
/**
 * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
    //add_image_size( 'photo', 148, 148, true );
}


/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

add_action('test', 'test');
function test( $a ){
    return '1';
}

add_filter('test', 'test2');
function test2( $a ){
    return '2';
}

class TextWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct("text_widget_vrv", "Text widget test 1.0",
            array("description" => "VAndolyak RV test widget"));
    }

    public function form( $instance)
    {
        $title = "";
        $count = "";

        if ( ! empty($instance) ) {
            $title = $instance["title"];
            $taxonomy = $instance["taxonomy"];
        }

        $titleId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");

        $taxonomyId = $this->get_field_id("taxonomy");
        $taxonomyName = $this->get_field_name("taxonomy");
        ?>
        <label for="<?php echo $titleId ?> ">Title </label><br>
        <input id="<?php echo $titleId ?>" type="text" name="<?php echo $tableName ?>" value="<?php echo $title ?>"><br>

        <label for="<?php echo $taxonomyId ?>">Taxonomies </label><br>
        <select name="<?php echo $taxonomyName?>" id="<?php echo $taxonomyId ?>" class="widefat">
            <?php foreach (get_taxonomies(['show_ui' => true, 'show_in_nav_menus' => true, '_builtin' => true], 'object') as $taxonomyItem):?>
                <option value="<?php echo $taxonomyItem->name?>" <?php if($taxonomyItem == $taxonomy) echo "selected"?>"><?php echo $taxonomyItem->label ?></option>
            <?php endforeach;?>
        </select>

        // continue limit field **
        <label for="<?php echo $titleId ?> ">Number limit </label><br>
        <input id="<?php echo $titleId ?>" type="text" name="<?php echo $tableName ?>" value="<?php echo $title ?>"><br>
        <?php
    }

    public function update( $newInstance, $oldInstance )
    {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["taxonomy"] = htmlentities($newInstance["taxonomy"]);
        $values["limit"] = htmlentities($newInstance["limit"]);//
        return $values;
    }

    public function widget( $args, $instance )
    {



        $title = $instance["title"];
        $taxonomy = $instance["taxonomy"];
        $number = $instance["limit"];

        $terms = get_terms( array(
            'taxonomy' => $taxonomy,
            'number' => $number,
            'hide_empty' => false,
        ) );
        var_dump($terms);

        echo "<h2>$title</h2>";
        echo "<p>$terms</p>";
        foreach ($terms->name as $name) {

        }
    }

}
add_action("widgets_init", function () {
    register_widget("TextWidget");
});

//public function widget( $args, $instance ) {
//    $title = apply_filters( 'widget_title', $instance[ 'title' ] );
//    $blog_title = get_bloginfo( 'name' );
//    $tagline = get_bloginfo( 'description' );
//    echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title']; ?>
<!---->
<!--    <p><strong>Site Name:</strong> --><?php //echo $blog_title ?><!--</p>-->
<!--    <p><strong>Tagline:</strong> --><?php //echo $tagline ?><!--</p>-->
<!---->
<!--    --><?php //echo $args['after_widget'];
//}